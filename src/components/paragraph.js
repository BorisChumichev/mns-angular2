import { Component, View, NgFor, NgClass, EventEmitter } from 'angular2/angular2'
import NotesList from './notes-list'
import utils from '../utils'

@Component({
  selector: '.paragraph',
  properties:
    [ 'paragraph', 'notes', 'focus'
    , 'fade', 'form', 'highlight', 'usersel' ]
})
@View({
  directives: [ NotesList, NgFor, NgClass ],
  template: `
    <p id="{{'paragraph' + paragraph.id}}">{{text}}</p>
    <div
      [notes]="notes"
      [hide]="!focus"
      [form]="form && focus"
      [origin]="paragraph.id"
      [ng-class]="{
        'notesList_collapsed': !focus,
        'notesList_faded': fade
      }"
      class="notesList"></div>
  `
})
export default class Paragraph {
  onInit() {
    this.text = this.paragraph.text
  }

  onChanges(changes) {
    if (this.getParagraphEl())
      this.updateHighlighting(changes)
  }

  /**
   * Modify displaing text according to highlightion
   * @param  {[type]} changes [description]
   * @return {[type]}         [description]
   */
  updateHighlighting(changes) {
    let highlight = _.get(changes, 'highlight.currentValue')
      , highlightRangeLength = _.get(highlight, 'range.length')
      , userSelection = _.get(changes, 'usersel.currentValue')

      this.getParagraphEl().innerHTML = (highlightRangeLength && this.focus)
        ? utils.wrapTextInSpan(this.paragraph.text, highlight)
        : (userSelection && this.focus)
          ? utils.wrapTextInSpan(
              this.paragraph.text,
              utils.buildRangeForUS(changes.usersel.currentValue)
            )
          : this.text
  }

  /**
   * Caches paragraph DOM node
   * @return {[type]} [description]
   */
  getParagraphEl() {
    if (!this.paragraphElement)
      this.paragraphElement = document.getElementById(`paragraph${this.paragraph.id}`)
    return this.paragraphElement
  }
}
