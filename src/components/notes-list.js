import { Component, View, NgFor, NgClass, NgIf, EventEmitter } from 'angular2/angular2'
import Note from './note'
import { default as NoteModel }  from '../models/Note'
import
  { focusOnParagraph, dropFocus
  , showForm, addNote
  , highlightText, flushHighlight
  , hideForm } from '../actions'

@Component({
  selector: '.notesList',
  properties: [ 'notes', 'hide', 'form', 'origin' ]
})
@View({
  directives: [ Note, NgFor, NgIf, NgClass ],
  template: `
    <div (click)="focusPar()" *ng-if="notes.length" class='notesList-counter'>{{notes.length}}</div>

    <div [ng-class]="{'paragraphNavigation_hidden': hide}" class="paragraphNavigation">
      <div (click)="onShowForm(input)"
        class="paragraphNavigation-item">Add note</div>
      <div (click)="blurPar()"
        class="paragraphNavigation-item">Hide notes</div>
    </div>

    <textarea
      #input
      id="{{'input' + origin}}"
      class="notesList-input"
      [ng-class]="{'notesList-sbmt_hidden': inputIsHidden}"
      (keyup)="onInputChange(input.value)"
      placeholder='Write a note and press enter…'></textarea>
    <div
      (click)="onPublishNewNote(input)"
      class="notesList-sbmt"
      [ng-class]="{'notesList-sbmt_hidden': submitIsHidden}" >
      Publish note 
    </div>

    <div 
      *ng-for="#note of notes"
      class="note"
      [ng-class]="{'note_hidden': hide, 'note_new': isNewNote(note) && !hide}"
      (mouseover)="onNoteHover(note)"
      (mouseleave)="onNoteLeave(note)"
      [date]="note.date"
      [author]="note.author"
      [text]="note.text" />
  `
})
export default class NotesList {
  onInit() {
    this.inputIsHidden = true
    this.submitIsHidden = true
  }

  /**
   * Subscribe on form state in store
   * @param  {object} changes
   */
  onChanges(changes) {
    _.get(changes, 'form.currentValue')
      ? this.showForm()
      : this.hideForm()
  }

  /**
   * Check wether note is new
   * @param  {Note} note
   */
  isNewNote(note) {
    return note.isNew()
  }

  /**
   * Reveal form
   */
  showForm() {
    this.inputIsHidden = false
    _.result(this.getInput(), 'focus')
  }

  /**
   * Reset form to initial state
   */
  hideForm() {
    this.inputIsHidden = true
    this.submitIsHidden = true
    _.set(this.getInput(), 'value', '')
    _.result(this.getInput(), 'blur')
  }

  /**
   * Cache input DOM node
   * @return {DOMInput}
   */
  getInput() {
    if (!this.input)
      this.input = document.getElementById(`input${this.origin}`)
    return this.input
  }

  /**
   * Tell store that form is shon
   */
  onShowForm() {
    store.dispatch(showForm())
  }

  /**
   * Add new note to the store
   * @param  {DOMInput} input 
   */
  onPublishNewNote(input) {
    let note = new NoteModel(
      input.value,
      this.origin,
      store.getState().userSelection
    )
    store.dispatch(addNote(note))
    this.hideForm()
  }

  /**
   * Highlight text selection when mouse is on note
   * @param  {array} selection.range
   */
  onNoteHover({ range }) {
    if (!store.getState().userSelection && this.inputIsHidden)
      _.get(range, 'length') && store.dispatch(
        highlightText({ range, origin: this.origin })
      )
  }

  /**
   * Disable text highlighting when mouse leaves note
   * @param  {array} selection.range
   */
  onNoteLeave({ range }) {
    if (!store.getState().userSelection && this.inputIsHidden)
      store.dispatch(flushHighlight())
  }

  /**
   * Hide submit button if user input is not valid
   * @param  {string} text input value
   */
  onInputChange(text) {
    this.submitIsHidden = !NoteModel.validText(text)
  }

  /**
   * Tell store that this parargraph is now in focus
   */
  focusPar() {
    store.dispatch(focusOnParagraph(this.origin))
  }

  /**
   * Tell store that this paragraph is't in focus
   * @return {[type]} [description]
   */
  blurPar() {
    store.dispatch(dropFocus())
  }
}
