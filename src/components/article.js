import mock from '../mock'
import notes from '../notes-mock'
import Paragraph from './paragraph'
import NotesList from './notes-list'
import Note from '../models/Note'
import utils from '../utils'
import { Component, View, NgFor, NgClass } from 'angular2/angular2'
import
  { focusOnParagraph, dropFocus
  , setSelection, clearSelection, showForm
  , addNote, setUserSelection
  , removeUserSelection } from '../actions'

@Component({
  selector: '.writing',
})
@View({
  directives: [ NgFor, NotesList, NgClass, Paragraph ],
  template: `
    <div
      #paragraphElement
      *ng-for="#paragraph of paragraphs"
      [paragraph]="paragraph"
      [notes]="paragraph.notes"
      [form]="notesForm"
      [focus]="paragraphFocus == paragraph.id"
      [fade]="paragraphFocus && (paragraphFocus != paragraph.id)"
      [highlight]="highlight"
      [usersel]="userSelection"
      (click)="stopPropagationIfFocused($event, paragraphFocus == paragraph.id)"
      (mouseup)="handleSelection($event, paragraph.id)"
      [ng-class]="{'paragraph_faded': paragraphFocus && paragraphFocus != paragraph.id}"
      class='paragraph'>
    </div>
    <div
      [style.display]="cursorStyles.display"
      [style.margin-left]="cursorStyles.marginLeft"
      [style.top]="cursorStyles.top"
      (click)="handleCursorClick($event)" class="cursor">Write a note</div>
  `
})
export default class Article {
  onInit() {
    utils.linkToTheStore(this)
    this.handleClickBubbling()

    //import mocks
    this.paragraphs = mock.map((text, i) => ({ text, id: i }))
    notes.forEach(
      note => store.dispatch(addNote(new Note(note.text, note.origin, note.selection)))
    )
  }

  /**
   * Loose focus and selection when user clicks
   * outside of focused element
   */
  handleClickBubbling() {
    document.body.onclick = () => {
      if (this.paragraphFocus !== null) {
        store.dispatch(dropFocus())
        store.dispatch(removeUserSelection())
      }
      if (window.getSelection().isCollapsed)
        store.dispatch(clearSelection())
    }
  }

  /**
   * Place "Write a note" popup
   */
  updateCursorStyles() {
    this.cursorStyles = (this.selection && !this.userSelection)
      ? { top: `${this.selection.top - 24}px`
        , display: 'block'
        , marginLeft: (this.selection.hcenter + 80 - (document.body.clientWidth * .5)) + 'px' }
      : { display: 'none' }
  }

  /**
   * Stop bubbling for click on focused paragraph
   * @param  {DOMEvent}  evt
   * @param  {Boolean} isFocused
   */
  stopPropagationIfFocused(evt, isFocused) {
    isFocused && evt.stopPropagation()
  }

  /**
   * Notify store when user selects text
   * @param  {Event target} options.target
   * @param  {number} paragraphId
   */
  handleSelection({ target }, paragraphId) {
    const paragraphElement = target 
      , sel = window.getSelection()
      , hasParagraphAsParent = sel.anchorNode.parentElement === paragraphElement
      , isInOneNode = sel.anchorNode === sel.focusNode
      , isNotTooSmall = Math.abs((sel.anchorOffset - sel.focusOffset)) > 2

    if (isInOneNode && isNotTooSmall && hasParagraphAsParent) {
      sel.paragraphId = paragraphId
      const bdRect = sel.getRangeAt(0).getBoundingClientRect()
      sel.hcenter = bdRect.width * .5 + bdRect.left
      sel.top = window.scrollY + bdRect.top
      store.dispatch(setSelection(sel))
    }
  }

  /**
   * Start note writing flow when popover is clicked
   * @param  {DOMEvent} evt
   */
  handleCursorClick(evt) {
    const { selection } = this
      , { dispatch } = store

    if (this.paragraphFocus !== selection.paragraphId)
      dispatch(focusOnParagraph(selection.paragraphId))

    dispatch(showForm())
    dispatch(setUserSelection(selection))
    evt.stopPropagation()
  }
}
