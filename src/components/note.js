import { Component, View } from 'angular2/angular2'

@Component({
  selector: '.note',
  properties: ['date', 'text', 'author']
})
@View({
  directives: [],
  template: `
    <div class="note-coordinates">{{date | date:"MM/dd/yy"}} by {{author}}</div>
    <div class="note-text">{{text}}</div>
  `
})
export default class Note {
}
