import 'zone.js/lib/browser/zone-microtask'
import 'reflect-metadata'
import 'babel-core/polyfill'
import { bootstrap } from 'angular2/angular2'
import Article from './components/article'
import writingApp from './reducers'
import { createStore } from 'redux'
import _ from 'lodash'
window._ = _
window.store = createStore(writingApp)
document.getElementById('loader')
document
  .getElementById('loader')
  .parentNode
  .removeChild(document.getElementById('loader'))
bootstrap(Article, [])
