export default {
  /**
   * Extends object with references to
   * root appstate nodes
   * @param  {object} obj
   */
  linkToTheStore(obj) {
    store.subscribe(
      () => {
        _.extend(obj, store.getState())
        obj.updateCursorStyles()
        obj.paragraphs.forEach(
          paragraph =>
            paragraph.notes = _(obj.notes)
              .filter({ origin: paragraph.id })
              .sortBy('date')
              .reverse()
              .value()
        )
      }
    )
  },

  /**
   * Wraps substring defined by range in span with className
   * @param  {string} text
   * @param  {array} options.range
   * @param  {string} className
   * @return {string}
   */
  wrapTextInSpan(text, { range }, className='paragraph-highlight') {
    if (_.get(range, 'length')) {
      range = _.sortBy(range, n => n)
      return `${text.slice(0, range[0])}<span class='${className}'>${text.slice(range[0], range[1])}</span>${text.slice(range[1])}`
    } else {
      return text
    }
  },

  /**
   * Builds range obj from selection
   * @param  {selection} us
   * @return {array}
   */
  buildRangeForUS(us) {
    return { range: [ us.anchorOffset, us.focusOffset ] }
  }
}
