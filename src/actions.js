/*
 * action types
 */

//Focused paragraph
export const FOCUS_ON_PARAGRAPH = 'FOCUS_ON_PARAGRAPH'
export const DROP_FOCUS = 'DROP_FOCUS'

//Notes
export const ADD_NOTE = 'ADD_NOTE'

//Text selection
export const SET_SELECTION = 'SET_SELECTION'
export const CLEAR_SELECTION = 'CLEAR_SELECTION'

//Note form
export const SHOW_FORM = 'SHOW_FORM'
export const HIDE_FORM = 'HIDE_FORM'

//Highlight
export const HIGHLIGHT_TEXT = 'HIGHLIGHT_TEXT'
export const FLUSH_HIGHLIGHT = 'FLUSH_HIGHLIGHT'

//User selection buffer
export const REMOVE_USER_SELECTION = 'REMOVE_USER_SELECTION'
export const SET_USER_SELECTION = 'SET_USER_SELECTION'


/*
 * action creators
 */

//Focused paragraph
export function focusOnParagraph(id) {
  return { type: FOCUS_ON_PARAGRAPH, id }
}

export function dropFocus() {
  return { type: DROP_FOCUS }
}

//Notes
export function addNote(note) {
  return { type: ADD_NOTE, note }
}

//Text selection
export function setSelection(selection) {
  return { type: SET_SELECTION, selection }
}

export function clearSelection() {
  return { type: CLEAR_SELECTION }
}

//Note form
export function showForm() {
  return { type: SHOW_FORM }
}

export function hideForm() {
  return { type: HIDE_FORM }
}

//Highlight
export function highlightText(highlight) {
  return { type: HIGHLIGHT_TEXT, highlight }
}

export function flushHighlight() {
  return { type: FLUSH_HIGHLIGHT }
}

//User selection buffer
export function setUserSelection(selection) {
  return { type: SET_USER_SELECTION, selection }
}

export function removeUserSelection() {
  return { type: REMOVE_USER_SELECTION }
}
