export default [
  {
    type: 'note',
    origin: 2,
    selection: { anchorOffset: 0, focusOffset: 191 },
    date: 1448209260226,
    author: 'boris362@yandex.ru',
    text: 'On some pages the sentences all begin with ‘And.’ I can’t see the point of this. Presumably you are aiming at producing an effect of panting continuity. Take out all the ‘Ands’ and see if it makes any difference.'
  },
  {
    type: 'note',
    origin: 2,
    selection: { anchorOffset: 0, focusOffset: 14 },
    date: 1448209260226,
    author: 'boris362@yandex.ru',
    text: 'Well written piece.'
  },
  {
    type: 'note',
    origin: 2,
    selection: { anchorOffset: 34, focusOffset: 57 },
    date: 1448209260226,
    author: 'boris362@yandex.ru',
    text: 'I can’t stop reading this!'
  },
  {
    type: 'note',
    origin: 3,
    selection: { anchorOffset: 35, focusOffset: 56 },
    date: 1448209260226,
    author: 'boris362@yandex.ru',
    text: 'That’s my favourite part! 🐋'
  },
  {
    type: 'note',
    origin: 3,
    selection: { anchorOffset: 30, focusOffset: 40 },
    date: 1448209260226,
    author: 'boris362@yandex.ru',
    text: 'Oh, this comment looks good.'
  }
]
