import { combineReducers } from 'redux'
import 'lodash'
import
  { FOCUS_ON_PARAGRAPH
  , DROP_FOCUS
  , ADD_NOTE
  , SET_SELECTION
  , CLEAR_SELECTION
  , SHOW_FORM
  , HIGHLIGHT_TEXT
  , FLUSH_HIGHLIGHT
  , REMOVE_USER_SELECTION
  , SET_USER_SELECTION
  , HIDE_FORM } from './actions'

function paragraphFocus(state = null, action) {
  switch (action.type) {
    case FOCUS_ON_PARAGRAPH:
      return action.id
    case DROP_FOCUS:
      return null
    default:
      return state
  }
}

function notes(state = [], action) {
  switch (action.type) {
    case ADD_NOTE:
      return [ ...state, action.note ]
    default:
      return state
  }
}

function selection(state = null, action) {
  switch (action.type) {
    case SET_SELECTION:
      return _.pick(action.selection
        , 'paragraphId'
        , 'isCollapsed'
        , 'anchorOffset'
        , 'focusOffset'
        , 'hcenter'
        , 'top'
      )
    case SET_USER_SELECTION:
    case CLEAR_SELECTION:
      return null
    default:
      return state
  }
}

function notesForm(state = false, action) {
  switch (action.type) {
    case SHOW_FORM:
      return true
    case HIDE_FORM:
    case ADD_NOTE:
    case DROP_FOCUS:
      return false
    default:
      return state
  }
}

function highlight(state = null, action) {
  switch (action.type) {
    case HIGHLIGHT_TEXT:
      return action.highlight
    case SET_USER_SELECTION:
    case FLUSH_HIGHLIGHT:
      return null
    default:
      return state
  }
}

function userSelection(state = null, action) {
  switch (action.type) {
    case SET_USER_SELECTION:
      return _.extend({}, action.selection)
    case ADD_NOTE:
    case DROP_FOCUS:
    case REMOVE_USER_SELECTION:
      return null
    default:
      return state
  }
}

const writingApp = combineReducers(
  { paragraphFocus
  , notes
  , selection
  , highlight
  , userSelection
  , notesForm }
)

export default writingApp
