const IS_NEW_THRESHOLD = 100
  , NOTE_MIN_LENGTH = 6
  , NOTE_MAX_LENGTH = 120

export default class Note {
  constructor(text, origin, selection, author='you') {
    _.extend(this,
      { origin , author
      , text: text.trim()
      , date: Date.now()
      , range: this._getRangeForSelection(selection) }
    )
  }

  /**
   * Check notes freshness
   * @return {Boolean}
   */
  isNew() {
    return Date.now() - this.date < IS_NEW_THRESHOLD 
  }
  
  /**
   * Convert selection object to range array
   * @param  {Selection} selection
   * @return {array}
   */
  _getRangeForSelection(selection) {
    return selection
      ? [ selection.anchorOffset, selection.focusOffset ]
      : []
  }
}

/**
 * Validates given string
 * @param  {string} text
 */
Note.validText = function (text) {
  let isNotTooShort = text.trim().length > NOTE_MIN_LENGTH
    , isNotTooLong = text.trim().length < NOTE_MAX_LENGTH
  return isNotTooShort && isNotTooLong
}
